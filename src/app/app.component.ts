import { Component } from '@angular/core';
import { Account } from './utils/account';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'up-and-coming-bank';

  ngOnInit(): void {
    // if (localStorage.getItem('accounts') == null) {
    // }
    var accounts = [];
    accounts[0] = new Account("afrosoft", "current", 453222);
    localStorage.setItem("accounts", JSON.stringify(accounts));
  }
}
