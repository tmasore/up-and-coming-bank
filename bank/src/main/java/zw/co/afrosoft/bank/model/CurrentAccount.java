package zw.co.afrosoft.bank.model;

import java.math.BigDecimal;

import javax.persistence.Entity;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Entity
@ConfigurationProperties(ignoreUnknownFields = true,prefix="app")
@Component
public class CurrentAccount extends Account {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Value("${app.overdraft_limit}")
	private BigDecimal OVERDRAFT_LIMIT=new BigDecimal(100000);
	
	public CurrentAccount() {
		
	}

	public BigDecimal getOVERDRAFT_LIMIT() {
		return OVERDRAFT_LIMIT;
	}

	public void setOVERDRAFT_LIMIT(BigDecimal oVERDRAFT_LIMIT) {
		OVERDRAFT_LIMIT = oVERDRAFT_LIMIT;
	}

	public BigDecimal deposit(BigDecimal depositAmt) {
		setBalance(getBalance().add(depositAmt));
		return getBalance();
	}
	
	public BigDecimal withdraw(BigDecimal withdrawalAmt) throws InsufficientFundsException {
		if(withdrawalAmt.compareTo(getBalance().add(OVERDRAFT_LIMIT))>0) {
			throw new InsufficientFundsException("Proposed withdrawal greater than balance plus overdraft available");
		}
		setBalance(getBalance().subtract(withdrawalAmt));
		return getBalance();
	}

	@Override
	public String toString() {
		super.toString();
		return "CurrentAccount [OVERDRAFT_LIMIT=" + OVERDRAFT_LIMIT + "]";
	}
	
	
}
