package zw.co.afrosoft.bank.model;

public enum TransactionType {
	WITHDRAW,DEPOSIT,TRANSFER
}
