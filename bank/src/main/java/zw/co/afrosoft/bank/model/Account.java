package zw.co.afrosoft.bank.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
@MappedSuperclass

public abstract class Account extends AuditableEntity implements AccountInterface{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long number;
	private BigDecimal balance;
	private User user;
	private AccountType accountType;
	
	public Account() {
		// TODO Auto-generated constructor stub
	}
	
	public Account(Long number, BigDecimal balance, User user) {
		super();
		this.number = number;
		this.balance = balance;
		this.user = user;
	}

	public Long getNumber() {
		return number;
	}

	public void setNumber(Long number) {
		this.number = number;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	@ManyToOne
	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	@Enumerated(EnumType.STRING)
	public AccountType getAccountType() {
		return accountType;
	}

	public void setAccountType(AccountType accountType) {
		this.accountType = accountType;
	}

	@Override
	public String toString() {
		return "Account [number=" + number + ", balance=" + balance + ", user=" + user + "]";
	}
	
	
}
