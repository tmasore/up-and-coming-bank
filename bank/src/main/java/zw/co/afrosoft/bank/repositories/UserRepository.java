package zw.co.afrosoft.bank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import zw.co.afrosoft.bank.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
