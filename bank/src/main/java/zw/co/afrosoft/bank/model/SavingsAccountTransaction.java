package zw.co.afrosoft.bank.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class SavingsAccountTransaction {
	private Long id;
	private SavingsAccount savingsAccount;
	private Transaction transaction;
	private AccountRole accountRole;
	
	public SavingsAccountTransaction() {
		// TODO Auto-generated constructor stub
	}
	
	public SavingsAccountTransaction(SavingsAccount savingsAccount, Transaction transaction,
			AccountRole accountRole) {
		super();
		this.savingsAccount = savingsAccount;
		this.transaction = transaction;
		this.accountRole = accountRole;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne
	public SavingsAccount getAccount() {
		return savingsAccount;
	}

	public void setAccount(SavingsAccount account) {
		this.savingsAccount = account;
	}
	@ManyToOne
	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	@Enumerated(EnumType.STRING)
	public AccountRole getAccountRole() {
		return accountRole;
	}

	public void setAccountRole(AccountRole accountRole) {
		this.accountRole = accountRole;
	}

	@Override
	public String toString() {
		return "SavingsAccountTransaction [id=" + id + ", savingsAccount=" + savingsAccount + ", transaction="
				+ transaction + ", accountRole=" + accountRole + "]";
	}
	
	
}
