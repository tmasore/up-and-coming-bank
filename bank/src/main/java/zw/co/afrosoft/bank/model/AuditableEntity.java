package zw.co.afrosoft.bank.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

@MappedSuperclass
public class AuditableEntity extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="date_created")
	@CreationTimestamp
	private LocalDateTime dateCreated;
	@Column(name="last_updated")
	@UpdateTimestamp
	private LocalDateTime lastUpdated;

	
}
