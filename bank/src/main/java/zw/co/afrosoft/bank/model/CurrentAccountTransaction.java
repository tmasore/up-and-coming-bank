package zw.co.afrosoft.bank.model;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="current_account_transaction")
public class CurrentAccountTransaction {
	private Long id;
	private CurrentAccount account;
	private Transaction transaction;
	private AccountRole accountRole;
	
	public CurrentAccountTransaction() {
		// TODO Auto-generated constructor stub
	}

	public CurrentAccountTransaction(CurrentAccount account2, Transaction transaction, AccountRole accountRole) {
		super();
		this.account = account2;
		this.transaction = transaction;
		this.accountRole = accountRole;
	}
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@ManyToOne
	public CurrentAccount getAccount() {
		return account;
	}

	public void setAccount(CurrentAccount account) {
		this.account = account;
	}
	@ManyToOne
	public Transaction getTransaction() {
		return transaction;
	}

	public void setTransaction(Transaction transaction) {
		this.transaction = transaction;
	}
	@Enumerated(EnumType.STRING)
	public AccountRole getAccountRole() {
		return accountRole;
	}

	public void setAccountRole(AccountRole accountRole) {
		this.accountRole = accountRole;
	}

	@Override
	public String toString() {
		return "AccountTransaction [id=" + id + ", account=" + account + ", transaction=" + transaction
				+ ", accountRole=" + accountRole + "]";
	}
	
}
