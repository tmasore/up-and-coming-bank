package zw.co.afrosoft.bank.model;

import javax.persistence.Embeddable;

@Embeddable
public class Address {
	private String addressLine1;
	private String addressLine2;
	private String city;//City should be an entity
	private String country;//Country should be an entity
	
	public Address() {
		// TODO Auto-generated constructor stub
	}

	public Address(String addressLine1, String addressLine2, String city, String country) {
		super();
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.country = country;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
