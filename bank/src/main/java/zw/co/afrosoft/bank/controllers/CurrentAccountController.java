package zw.co.afrosoft.bank.controllers;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.security.auth.login.AccountNotFoundException;

import org.hibernate.criterion.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.convert.JodaTimeConverters.LocalDateTimeToDateConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
//
//import org.springframework.web.bind.annotation.PostMapping;
//import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import zw.co.afrosoft.bank.model.Account;
import zw.co.afrosoft.bank.model.AccountInterface;
import zw.co.afrosoft.bank.model.AccountRole;
import zw.co.afrosoft.bank.model.AccountType;
import zw.co.afrosoft.bank.model.CurrentAccount;
import zw.co.afrosoft.bank.model.CurrentAccountTransaction;
import zw.co.afrosoft.bank.model.InsufficientFundsException;
import zw.co.afrosoft.bank.model.SavingsAccount;
import zw.co.afrosoft.bank.model.SavingsAccountTransaction;
import zw.co.afrosoft.bank.model.Transaction;
import zw.co.afrosoft.bank.model.TransactionType;
import zw.co.afrosoft.bank.model.User;
import zw.co.afrosoft.bank.repositories.CurrentAccountRepository;
import zw.co.afrosoft.bank.repositories.CurrentAccountTransactionRepository;
import zw.co.afrosoft.bank.repositories.SavingsAccountRepository;
import zw.co.afrosoft.bank.repositories.SavingsAccountTransactionRepository;
import zw.co.afrosoft.bank.repositories.TransactionRepository;
import zw.co.afrosoft.bank.repositories.UserRepository;

@RestController
@RequestMapping("/accounts/current")
public class CurrentAccountController {
	private CurrentAccountRepository currentAccountRepo;
	private CurrentAccountTransactionRepository currentAccountTransactionRepository;
	private TransactionRepository transactionRepository;
	private UserRepository userRepo;


	public CurrentAccountController(
			CurrentAccountRepository currentAccountRepo,
			CurrentAccountTransactionRepository currentAccountTransactionRepository,
			TransactionRepository transactionRepository, UserRepository userRepo) {
		super();
		
		this.currentAccountRepo = currentAccountRepo;
		this.currentAccountTransactionRepository = currentAccountTransactionRepository;
		this.transactionRepository = transactionRepository;
		this.userRepo = userRepo;
	}

	@PostConstruct
	@Autowired
	public void loadData() {
		ObjectMapper mapper = new ObjectMapper();
			try {
			CurrentAccount account= mapper.readValue(new File("data/account-data.json"), CurrentAccount.class);
			User unsavedUser=account.getUser();
			account.setOVERDRAFT_LIMIT(account.getOVERDRAFT_LIMIT());
			User user=userRepo.save(unsavedUser);
			account.setUser(user);
			currentAccountRepo.save(account);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	}
	
	
	@PostMapping
	@RequestMapping("/withdraw/{withdrawalAmt}")
	public AccountInterface withdraw(@RequestBody CurrentAccount account,@PathVariable BigDecimal withdrawalAmt) throws AccountNotFoundException{
		if(account.getAccountType().equals(AccountType.CURRENT)) {
			Optional<CurrentAccount> accountFromDb=currentAccountRepo.findById(account.getId());
			if(accountFromDb.isPresent()) {
				try {
					account=accountFromDb.get();
					
					account.withdraw(withdrawalAmt);
					currentAccountRepo.save((CurrentAccount)account);
					Transaction transaction=new Transaction(withdrawalAmt, LocalDateTime.now(), TransactionType.WITHDRAW);
					transaction=transactionRepository.save(transaction);
					CurrentAccountTransaction crntAccntTransaction=new CurrentAccountTransaction((CurrentAccount) account, transaction, AccountRole.SOURCE_ACCT);
					currentAccountTransactionRepository.save(crntAccntTransaction);
				} catch (InsufficientFundsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				throw new AccountNotFoundException("Account not found");
			}
			return account;
		}
		throw new AccountNotFoundException("Account not Current Account!");
		
	}
	
	@PostMapping
	@RequestMapping("/deposit/{depositAmt}")
	public AccountInterface deposit(@RequestBody CurrentAccount account,@PathVariable BigDecimal depositAmt) throws AccountNotFoundException{
		if(account.getAccountType()==AccountType.CURRENT) {
			Optional<CurrentAccount> accountFromDb=currentAccountRepo.findById(account.getId());
			if(accountFromDb.isPresent()) {
					account=accountFromDb.get();
					
					account.deposit(depositAmt);
					currentAccountRepo.save((CurrentAccount)account);
					Transaction transaction=new Transaction(depositAmt, LocalDateTime.now(), TransactionType.DEPOSIT);
					transaction=transactionRepository.save(transaction);
					CurrentAccountTransaction crntAccntTransaction=new CurrentAccountTransaction((CurrentAccount) account, transaction, AccountRole.SOURCE_ACCT);
					currentAccountTransactionRepository.save(crntAccntTransaction);
				
			}
			else {
				throw new AccountNotFoundException("Account not found");
			}
			return account;
		}
		throw new AccountNotFoundException("Account not Current Account!");
		
	}
	
	@PostMapping
	@RequestMapping("/transfer/{transferAmt}")
	public AccountInterface transfer(@RequestBody CurrentAccount sourceAccount,@RequestBody CurrentAccount destinationAccount,@PathVariable BigDecimal transferAmt) throws AccountNotFoundException, InsufficientFundsException{
		if(sourceAccount.getAccountType()==AccountType.CURRENT&&destinationAccount.getAccountType()==AccountType.CURRENT) {
			Optional<CurrentAccount> sourceAccountFromDb=currentAccountRepo.findById(sourceAccount.getId());
			Optional<CurrentAccount> destinationAccountFromDb=currentAccountRepo.findById(destinationAccount.getId());
			if(sourceAccountFromDb.isPresent()&&destinationAccountFromDb.isPresent()) {
					sourceAccount=sourceAccountFromDb.get();
					destinationAccount=destinationAccountFromDb.get();
					sourceAccount.withdraw(transferAmt);
					destinationAccount.deposit(transferAmt);
					currentAccountRepo.save((CurrentAccount)sourceAccount);
					currentAccountRepo.save((CurrentAccount)destinationAccount);
					Transaction transaction=new Transaction(transferAmt, LocalDateTime.now(), TransactionType.TRANSFER);
					transaction=transactionRepository.save(transaction);
					CurrentAccountTransaction crntAccntTransaction=new CurrentAccountTransaction((CurrentAccount) sourceAccount, transaction, AccountRole.SOURCE_ACCT);
					CurrentAccountTransaction crntAccntTransaction2=new CurrentAccountTransaction((CurrentAccount) destinationAccount, transaction, AccountRole.DESTINATION_ACCT);
					currentAccountTransactionRepository.save(crntAccntTransaction);
					currentAccountTransactionRepository.save(crntAccntTransaction2);
				
			}
			else {
				throw new AccountNotFoundException("Account not found");
			}
			return destinationAccount;
		}
		throw new AccountNotFoundException("Account not Current Account!");
		
	}


}
