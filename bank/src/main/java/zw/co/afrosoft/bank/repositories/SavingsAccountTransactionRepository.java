package zw.co.afrosoft.bank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import zw.co.afrosoft.bank.model.SavingsAccountTransaction;

public interface SavingsAccountTransactionRepository extends JpaRepository<SavingsAccountTransaction, Long>{

}
