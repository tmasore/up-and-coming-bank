package zw.co.afrosoft.bank.model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Transaction {

	private Long Id;
	private BigDecimal amount;
	private LocalDateTime date;
	private TransactionType transactionType;
	private Long transactionId;
	
	public Transaction() {
		// TODO Auto-generated constructor stub
	}

	public Transaction(BigDecimal amount, LocalDateTime date, TransactionType transactionType) {
		super();
		this.amount = amount;
		this.date = date;
		this.transactionType = transactionType;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	@Enumerated
	public TransactionType getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(TransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public Long getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	@Override
	public String toString() {
		return "Transaction [Id=" + Id + ", amount=" + amount + ", date=" + date + ", transactionType="
				+ transactionType + "]";
	}
}
