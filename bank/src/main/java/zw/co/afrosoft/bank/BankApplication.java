package zw.co.afrosoft.bank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import zw.co.afrosoft.bank.model.CurrentAccount;
import zw.co.afrosoft.bank.model.SavingsAccount;

@SpringBootApplication
public class BankApplication {

	public static void main(String[] args) {
		ConfigurableApplicationContext cntxt=SpringApplication.run(BankApplication.class, args);
		SavingsAccount accnt=cntxt.getBean("savingsAccount",SavingsAccount.class);
		CurrentAccount account=cntxt.getBean("currentAccount",CurrentAccount.class);
		System.out.println("...............................................");
		System.out.println(accnt);
		System.out.println(account);
		System.out.println("................................................");
	}

}
