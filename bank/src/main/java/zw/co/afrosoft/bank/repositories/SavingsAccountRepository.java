package zw.co.afrosoft.bank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import zw.co.afrosoft.bank.model.SavingsAccount;

public interface SavingsAccountRepository extends JpaRepository<SavingsAccount, Long>{

}
