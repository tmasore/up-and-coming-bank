package zw.co.afrosoft.bank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import zw.co.afrosoft.bank.model.CurrentAccount;
public interface CurrentAccountRepository extends JpaRepository<CurrentAccount, Long>{

}
