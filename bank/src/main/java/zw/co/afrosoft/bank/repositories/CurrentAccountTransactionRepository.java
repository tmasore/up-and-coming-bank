package zw.co.afrosoft.bank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import zw.co.afrosoft.bank.model.CurrentAccountTransaction;

public interface CurrentAccountTransactionRepository extends JpaRepository<CurrentAccountTransaction, Long>{

}
