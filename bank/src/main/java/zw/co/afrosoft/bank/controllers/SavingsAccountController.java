package zw.co.afrosoft.bank.controllers;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.security.auth.login.AccountNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import zw.co.afrosoft.bank.model.AccountInterface;
import zw.co.afrosoft.bank.model.AccountRole;
import zw.co.afrosoft.bank.model.AccountType;
import zw.co.afrosoft.bank.model.CurrentAccount;
import zw.co.afrosoft.bank.model.CurrentAccountTransaction;
import zw.co.afrosoft.bank.model.InsufficientFundsException;
import zw.co.afrosoft.bank.model.SavingsAccount;
import zw.co.afrosoft.bank.model.SavingsAccountTransaction;
import zw.co.afrosoft.bank.model.Transaction;
import zw.co.afrosoft.bank.model.TransactionType;
import zw.co.afrosoft.bank.model.User;
import zw.co.afrosoft.bank.repositories.SavingsAccountRepository;
import zw.co.afrosoft.bank.repositories.SavingsAccountTransactionRepository;
import zw.co.afrosoft.bank.repositories.TransactionRepository;
import zw.co.afrosoft.bank.repositories.UserRepository;

@RestController
@RequestMapping("/accounts/savings")
public class SavingsAccountController {
	private SavingsAccountTransactionRepository savingsAccountTransactionRepository;
	private SavingsAccountRepository savingsAccountRepository;
	private TransactionRepository transactionRepository;
	private UserRepository userRepo;
	public SavingsAccountController(SavingsAccountTransactionRepository savingsAccountTransactionRepository,
			SavingsAccountRepository savingsAccountRepository,
			TransactionRepository transactionRepository, UserRepository userRepo) {
		super();
		this.savingsAccountTransactionRepository = savingsAccountTransactionRepository;
		this.savingsAccountRepository = savingsAccountRepository;
		this.transactionRepository = transactionRepository;
		this.userRepo = userRepo;
	}
	
	@PostConstruct
	@Autowired
	public void loadData() {
		ObjectMapper mapper = new ObjectMapper();
			try {
			SavingsAccount account= mapper.readValue(new File("data/account-data2.json"), SavingsAccount.class);
			User unsavedUser=account.getUser();
			User user=userRepo.save(unsavedUser);
			account.setUser(user);
			savingsAccountRepository.save(account);
			} catch (JsonParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JsonMappingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
	}
	
	@PostMapping
	@RequestMapping("/withdraw/{withdrawalAmt}")
	public AccountInterface withdraw(@RequestBody SavingsAccount account,@PathVariable BigDecimal withdrawalAmt) throws AccountNotFoundException{
		if(account.getAccountType().equals(AccountType.SAVINGS)) {
			Optional<SavingsAccount> accountFromDb=savingsAccountRepository.findById(account.getId());
			if(accountFromDb.isPresent()) {
				try {
					account=accountFromDb.get();
					
					account.withdraw(withdrawalAmt);
					savingsAccountRepository.save((SavingsAccount)account);
					Transaction transaction=new Transaction(withdrawalAmt, LocalDateTime.now(), TransactionType.WITHDRAW);
					transaction=transactionRepository.save(transaction);
					SavingsAccountTransaction savingsAccntTransaction=new SavingsAccountTransaction((SavingsAccount) account, transaction, AccountRole.SOURCE_ACCT);
					savingsAccountTransactionRepository.save(savingsAccntTransaction);
				} catch (InsufficientFundsException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			else {
				throw new AccountNotFoundException("Account not found");
			}
			return account;
		}
		throw new AccountNotFoundException("Account not Savings Account!");
		
	}

}
