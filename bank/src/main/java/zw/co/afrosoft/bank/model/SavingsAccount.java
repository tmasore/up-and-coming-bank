package zw.co.afrosoft.bank.model;

import java.math.BigDecimal;

import javax.persistence.Entity;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
@Entity
@ConfigurationProperties(ignoreUnknownFields = true,prefix="app")
@Component
public class SavingsAccount extends Account{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private BigDecimal MINIMUM_DEPOSIT;
	private BigDecimal MINIMUM_BALANCE;

	public SavingsAccount(@Value("${app.minimum_deposit}")BigDecimal mINIMUM_DEPOSIT, 
							@Value("${app.minimum_balance}")BigDecimal mINIMUM_BALANCE) {
		super();
		this.MINIMUM_DEPOSIT = mINIMUM_DEPOSIT;
		this.MINIMUM_BALANCE = mINIMUM_BALANCE;
		this.setBalance(new BigDecimal(1000));
	}

	
	
	public void setMINIMUM_BALANCE(BigDecimal mINIMUM_BALANCE) {
		MINIMUM_BALANCE = mINIMUM_BALANCE;
	}

	public BigDecimal getMINIMUM_DEPOSIT() {
		return MINIMUM_DEPOSIT;
	}

	public void setMINIMUM_DEPOSIT(BigDecimal mINIMUM_DEPOSIT) {
		this.MINIMUM_DEPOSIT=mINIMUM_DEPOSIT;
	}

	public BigDecimal getMINIMUM_BALANCE() {
		return MINIMUM_BALANCE;
	}
	
	public BigDecimal withdraw(BigDecimal withdrawalAmount) throws InsufficientFundsException {
		if(getBalance().compareTo(new BigDecimal(1000))<=0) {
			throw new InsufficientFundsException();
		}
		
		setBalance(getBalance().subtract(withdrawalAmount));
		return getBalance();
	}

	public BigDecimal deposit(BigDecimal depositAmt){
		setBalance(getBalance().add(depositAmt));
		return getBalance();
	}

	@Override
	public String toString() {
		return "SavingsAccount [MINIMUM_DEPOSIT=" + MINIMUM_DEPOSIT + ", MINIMUM_BALANCE=" + MINIMUM_BALANCE + "]";
	}

	
	
	
}
