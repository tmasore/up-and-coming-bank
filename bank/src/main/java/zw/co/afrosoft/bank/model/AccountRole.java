package zw.co.afrosoft.bank.model;

import javax.persistence.Table;

@Table(name="account_role")
public enum AccountRole {
	SOURCE_ACCT,DESTINATION_ACCT
}
