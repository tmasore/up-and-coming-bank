package zw.co.afrosoft.bank.model;

import java.math.BigDecimal;

public interface AccountInterface {

	AccountType getAccountType();

	Long getId();

	BigDecimal withdraw(BigDecimal withdrawalAmt) throws InsufficientFundsException;

}
