package zw.co.afrosoft.bank.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import zw.co.afrosoft.bank.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Long>{

}
