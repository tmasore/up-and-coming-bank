package zw.co.afrosoft.bank.model;

public class InsufficientFundsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InsufficientFundsException() {
		super("Insufficient Funds!");
		// TODO Auto-generated constructor stub
	}

	public InsufficientFundsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	
}
